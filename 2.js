function dividedNotX(nums,x) {
    let result = [];
    for(let i=0; i < nums.length; i+=1){
        let checker = false;
        for(let j=0; j < nums.length && !checker; j+=1){
            if(i!==j && (nums[i] / nums[j]) === x) {
                checker = true;
            }
        }
        if (!checker) result.push(nums[i]);
    }
    return result;
}

console.log(dividedNotX([1,2,3,4],4))