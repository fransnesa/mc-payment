function substractedNotZero(nums) {
    let result = [];
    for(let i=0; i < nums.length; i+=1){
        let checker = false;
        for(let j=0; j < nums.length && !checker; j+=1){
            if(i!==j && (nums[i] - nums[j]) < 0) {
                checker = true;
            }
        }
        if (!checker) result.push(nums[i]);
    }
    return result;
}

console.log(substractedNotZero([3,1,4,2]))